package main
import "bitbucket.org/skariuki/utube-go/utube"

func main() {

	var youtube utube.Youtube

	youtube.AddUrl(
		"https://www.youtube.com/watch?v=py7-UC8_TZE",
		"mp4",
		"720p")

	youtube.Download()
}
