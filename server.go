package main

import (
	"net"
	"fmt"
	"os"
)

// server to receive download links
// start server and listen to incoming requests
// Call the other utils; utube, and scraper

var VERSION = "1.0.1"

type Server struct {
	listen net.Listener

	host string
	port int
	conType string
}

// handle request
func  (server *Server) handleRequest(conn net.Conn) {
	buf := make([]byte, 1024)
	_, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Error reading:", err.Error())
	}

	conn.Write([]byte("utube " + VERSION));

	conn.Close()
}

// start the server
func (server *Server) start() {
	defer server.listen.Close()

	for {
		// Listen for an incoming connection.
		conn, err := server.listen.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}

		fmt.Printf("Received message %s -> %s \n", conn.RemoteAddr(), conn.LocalAddr())

		go server.handleRequest(conn)
	}
}

// create the server
func create(conType string, host string, port int) Server{
	server := Server{}

	// Add validation
	server.conType = conType
	server.host = host
	server.port = port

	address := fmt.Sprintf("%s:%d", server.host, server.port)
	listen, err := net.Listen(server.conType, address)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}

	server.listen = listen

	fmt.Println("Listening to " + address)
	return server
}

// stop the server
func (server *Server) stop() {
	server.listen.Close()
}

func main() {

	server := create("tcp", "127.0.0.1", 8000)
	server.start()
	// server.stop()
}
