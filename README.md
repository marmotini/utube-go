#UTUBE-GO

Based on https://github.com/nficano/pytube

## Features

- Download youtube videos
- Save and Organize the videos 
- Scrape web page

### To do packages

- Firefox plugin to bookmark videos for download
- save video to google music
- utube downloader - done
- Video marshal. organize videos to respective folders and sub folders

## Installation
```go

go get github.com/skariuki/utube-go

```

## Basic Usage

```go

package main
import "bitbucket.org/skariuki/utube-go/utube"

func main() {

	var youtube utube.Youtube

	youtube.AddUrl(
		"https://www.youtube.com/watch?v=py7-UC8_TZE",
		"mp4",
		"720p")

	youtube.Download()
}


```

See examples and benchmarks for more information..

## License

(The MIT License)

Copyright (c) 2015 Stanley Kariuki

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
