package utube

type Stats struct {

	TimeTaken int

	DownloadTime []byte

	Errors []error

}