package utube

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"os"
	"io"
	"strings"
	"encoding/json"
	"net/url"
	"regexp"
	"strconv"
)

const (
	js_code = false
	chunk_size = 8*1028
	force_overwrite = true

	paused = 1
	stopped= 2
	running= 3
)

type PassThroughRead struct {
	io.Reader

	total    int64 // Total # of bytes transferred
	length   int64 // Expected length
	progress float64

	state int
}

type RequestInfo struct {
	Url string
	codec string
	resolution string
}

type Video struct {
	Info []VideoInfo
	RequestInfo RequestInfo
	Filename string

	title string
}

type VideoInfo struct {
	Profile string // Simple
	Extension string // 3gp
	Video_bitrate string // 0.05
	Audio_bitrate string // 24
	Audio_codec string // AAC
	Video_codec string // MPEG-4 Visual
	Resolution string // 144p
	DownloadUrl string
}

func SetupVideo(url string, codec string, resolution string) (Video, error)  {

	video := new(Video)

	req := RequestInfo{
		codec: codec,
		resolution: resolution,
		Url: url,
	}

	video.RequestInfo = req

	err := video.videoInfo()

	return *video, err

}

func extractFmt(url string) (string, VideoInfo, error) {

	var fmtData = &VideoInfo{}
	var itag string

	results := regexp.MustCompile(`itag=(\d+)`).FindAllString(url, -1)
	itags := strings.Split(results[0], "=")

	if len(itags) > 0 {
		itag = itags[1]
		i, err := strconv.ParseInt(itag, 10, 32)
		if err != nil {
			return "", *fmtData, err
		}

		attr := YT_ENCODING[int32(i)]

		for i, value := range attr {
			key := YT_ENCODING_KEYS[i]

			err := SetField(fmtData, strings.Title(key), value)
			if err != nil {
				return "", *fmtData, err
			}
		}

	}

	return itag, *fmtData, nil

}

func (video *Video)  parseStreamMap(text string) map[string][]string {

	videos := make(map[string][]string)
	vid := strings.Split(text, ",")
	vids := make([]string, len(vid))

	var key string
	var value string

	for _, video := range vid {
		vids = append(vids, strings.Split(video, "&")...)
	}

	for _, video := range vids {

		kv := strings.Split(video, "=")

		if len(kv) > 1 {

			key = kv[0]
			value = kv[1]

			videos[key] = append(videos[key], value)

		}
	}

	return videos
}

func (video *Video) videoInfo() (err error) {

	res, err := http.Get(string(video.RequestInfo.Url))

	if err != nil {
		return err
	} else {
		content, err := ioutil.ReadAll(res.Body)

		if err != nil {
			return err
		} else {

			player_conf := content[18 + strings.Index(string(content), "ytplayer.config = "):]

			var data map[string]interface{}
			var jsUrl string
			var bracket_count int32
			var i int32
			var streamMap map[string][]string

			for  _, char := range player_conf {

				if char == '{' {
					bracket_count += 1
				} else if char == '}' {
					bracket_count -= 1
				}

				if bracket_count == 0 {
					break
				}

				i++
			}

			json.Unmarshal(player_conf[:(i + 1)], &data)

			args, found := data["args"].(map[string]interface{})
			if found {
				urlEncoding, found := args["url_encoded_fmt_stream_map"].(string)
				if found {
					streamMap = video.parseStreamMap(urlEncoding)
				} else {
					fmt.Println("Failed to get url encoding")
				}

				title, found := args["title"].(string)
				if found {
					video.title = title
				} else {
					fmt.Println("Failed to get title")
				}
			} else {
				fmt.Println("Failed to get args")
			}

			assets, found := data["assets"].(map[string]interface{})
			if found {
				js, found := assets["js"].(string)
				if found {
					jsUrl = "http:" + js
					fmt.Println(jsUrl)
				} else {
					fmt.Println("Failed to get js asset")
				}
			}

			videoUrls := streamMap["url"]
			for _, unEscUrl := range videoUrls {

				escUrl, err := url.QueryUnescape(unEscUrl)
				if err == nil {

					_, info, err := extractFmt(escUrl)
					if err == nil {
						info.DownloadUrl = escUrl
						video.Info = append(video.Info, info)
					} else {
						return err
					}

				}

			}

		}

	}

	return

}

func (video *Video) downloadInfo() (downloadPath string, info VideoInfo, err error) {

	var fileName string
	downloadDir, err := DefaultDir()

	if err != nil {
		return
	}

	if len(video.Info) > 0 {
		info = video.Info[0]
	}

	if video.Filename != "" {
		fileName = video.Filename
	} else {
		fileName = video.title
	}

	for _, vinfo := range video.Info {
		if video.RequestInfo.codec == info.Audio_codec && video.RequestInfo.resolution == info.Resolution  {
			info = vinfo
			break
		} else if (video.RequestInfo.codec == info.Audio_codec || video.RequestInfo.resolution == info.Resolution) {
			info = vinfo
		}
	}

	downloadPath = downloadDir + "/" + fileName + "." + info.Extension

	return
}

func (video *Video) SetFilename(filename string){

	video.Filename = filename

}

func (video *Video) Download() (err error) {

	path, info, err := video.downloadInfo()
	if err != nil {
		return fmt.Errorf("Could not fetch download info")
	}

	output, err := os.Create(path)
	if err != nil {
		return fmt.Errorf("Could not create file")
	}

	defer output.Close()

	response, err := http.Get(info.DownloadUrl)
	if err != nil {
		return fmt.Errorf("Could not get web page. %s", err)
	} else {
		defer response.Body.Close()

		pt := &PassThroughRead{
			Reader: response.Body,
			length: response.ContentLength,
		}

		_, err := io.Copy(output, pt)
		if err != nil {
			return fmt.Errorf("Could not download file")
		}

		fmt.Println("Finised")
	}

	return

}

func (passthur *PassThroughRead) Read(p []byte) (n int, err error) {

	n, err = passthur.Reader.Read(p)

	if n > 0 {
		passthur.total += int64(n)
		percentage := float64(passthur.total) / float64(passthur.length) * float64(100)

		// i := int(percentage)
		// is := fmt.Sprintf("%v \n", i)

		// fmt.Fprintf(os.Stderr, is)
		passthur.progress = percentage
	}

	return

}
