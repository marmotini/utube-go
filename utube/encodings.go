package utube

var YT_ENCODING = map[int32][]string{
	// Flash Video
	5: []string{"flv", "240p", "Sorenson H.263", "N/A", "0.25", "MP3", "64"},
	6: []string{"flv", "270p", "Sorenson H.263", "N/A", "0.8", "MP3", "64"},
	34: []string{"flv", "360p", "H.264", "Main", "0.5", "AAC", "128"},
	35: []string{"flv", "480p", "H.264", "Main", "0.8-1", "AAC", "128"},

	// 3GP
	36: []string{"3gp", "240p", "MPEG-4 Visual", "Simple", "0.17", "AAC", "38"},
	13: []string{"3gp", "N/A", "MPEG-4 Visual", "N/A", "0.5", "AAC", "N/A"},
	17: []string{"3gp", "144p", "MPEG-4 Visual", "Simple", "0.05", "AAC", "24"},

	// MPEG-4
	18: []string{"mp4", "360p", "H.264", "Baseline", "0.5", "AAC", "96"},
	22: []string{"mp4", "720p", "H.264", "High", "2-2.9", "AAC", "192"},
	37: []string{"mp4", "1080p", "H.264", "High", "3-4.3", "AAC", "192"},
	38: []string{"mp4", "3072p", "H.264", "High", "3.5-5", "AAC", "192"},
	82: []string{"mp4", "360p", "H.264", "3D", "0.5", "AAC", "96"},
	83: []string{"mp4", "240p", "H.264", "3D", "0.5", "AAC", "96"},
	84: []string{"mp4", "720p", "H.264", "3D", "2-2.9", "AAC", "152"},
	85: []string{"mp4", "1080p", "H.264", "3D", "2-2.9", "AAC", "152"},

	// WebM
	43: []string{"webm", "360p", "VP8", "N/A", "0.5", "Vorbis", "128"},
	44: []string{"webm", "480p", "VP8", "N/A", "1", "Vorbis", "128"},
	45: []string{"webm", "720p", "VP8", "N/A", "2", "Vorbis", "192"},
	46: []string{"webm", "1080p", "VP8", "N/A", "N/A", "Vorbis", "192"},
	100: []string{"webm", "360p", "VP8", "3D", "N/A", "Vorbis", "128"},
	101: []string{"webm", "360p", "VP8", "3D", "N/A", "Vorbis", "192"},
	102: []string{"webm", "720p", "VP8", "3D", "N/A", "Vorbis", "192"},
}

// The keys corresponding to the quality/codec map above.
var YT_ENCODING_KEYS = [...]string{
	"extension",
	"resolution",
	"video_codec",
	"profile",
	"video_bitrate",
	"audio_codec",
	"audio_bitrate",
}
