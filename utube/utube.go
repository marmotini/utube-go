package utube
import (
	"sync"
	"fmt"
)

type Youtube struct {

	videos []Video

}

func (youtube *Youtube) AddUrl(url string, codec string, resolution string) (video Video, err error) {

	video, err = SetupVideo(url, codec, resolution)

	youtube.videos = append(youtube.videos, video)

	return

}

// For test mockup only
func (youtube *Youtube) AddVideo(video Video) {

	youtube.videos = append(youtube.videos, video)

}

func (youtube *Youtube) Download() {

	var wg sync.WaitGroup

	wg.Add(len(youtube.videos))

	for _, video := range youtube.videos {

		go func(video Video) {

			defer wg.Done()

			err := video.Download()
			if err != nil{
				fmt.Println(err)
			}

		}(video)

	}

	wg.Wait()

	fmt.Println("Finished downloading all!")

}
