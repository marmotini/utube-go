package utube

import (
	"os"
	"os/user"
	"reflect"
	"fmt"
	"errors"
)

const DEFAULT_DIR = "utube"

func DefaultDir() (string, error) {

	me, _ := user.Current();

	home := me.HomeDir

	defaultDir := home + "/" + DEFAULT_DIR

	if _, err := os.Stat(defaultDir); err != nil {
		if os.IsNotExist(err) {
			os.Mkdir(defaultDir, 0777)
		}
	}

	return defaultDir, nil

}

func SetField(obj interface{}, name string, value interface{}) error {
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(name)

	if !structFieldValue.IsValid() {
		return fmt.Errorf("No such field: %s in obj", name)
	}

	if !structFieldValue.CanSet() {
		return fmt.Errorf("Cannot set %s field value", name)
	}

	structFieldType := structFieldValue.Type()
	val := reflect.ValueOf(value)
	if structFieldType != val.Type() {
		return errors.New("Provided value type didn't match obj field type")
	}

	structFieldValue.Set(val)

	return nil
}

func extractUri() ([]byte, error){

	return nil, nil

}

func safeFilename() {

}

